package com.natixis;

import org.junit.Test;

import static com.natixis.Cell.cell;
import static com.natixis.Players.*;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class TestTicTacToe {

    @Test
    public void NobodyWinsWhenNobodyPlayed() {
        // Given
        String board =
                "   \n" +
                "   \n" +
                "   \n";

        // When
        Winner actualWinner = parse(board).findWinner();


        // Then
        assertThat(actualWinner).isEqualTo(Winner.nobody());
    }

    @Test
    public void NobodyWinsWhenNoLinesFormed() {
        // Given
        String board =
                "XOX\n" +
                "OXX\n" +
                "OXO\n";

        // When
        Winner actualWinner = parse(board).findWinner();


        // Then
        assertThat(actualWinner).isEqualTo(Winner.nobody());
    }

    @Test
    public void XWinsWhenFillingTheThirdRowOnA3x3Board() {
        // Given
        String board =
                "   \n" +
                "   \n" +
                "XXX\n";

        // When
        Winner actualWinner = parse(board).findWinner();

        // Then
        assertThat(actualWinner).isEqualTo(Winner.player(xPlayer));
    }

    @Test
    public void OWinsWhenFillingTheFirstColumnOnA3x3Board() {
        // Given
        String board =
                "O  \n" +
                "O  \n" +
                "O  \n";

        // When
        Winner actualWinner = parse(board).findWinner();

        // Then
        assertThat(actualWinner).isEqualTo(Winner.player(oPlayer));
    }

    @Test
    public void XWinsWhenFillingTheSecondColumnOnA3x3Board() {
        // Given
        String board =
                " X \n" +
                " X \n" +
                " X \n";

        // When
        Winner actualWinner = parse(board).findWinner();

        // Then
        assertThat(actualWinner).isEqualTo(Winner.player(xPlayer));
    }

    @Test
    public void OWinsWhenFillingTheThirdColumnOnA3x3Board() {
        // Given
        String board =
                "  O\n" +
                "  O\n" +
                "  O\n";

        // When
        Winner actualWinner = parse(board).findWinner();

        // Then
        assertThat(actualWinner).isEqualTo(Winner.player(oPlayer));
    }

    @Test
    public void OWinsWhenFillingTheFirstDiagonaleOnA3x3Board() {
        // Given
        String board =
                "O  \n" +
                " O \n" +
                "  O\n";

        // When
        Winner actualWinner = parse(board).findWinner();

        // Then
        assertThat(actualWinner).isEqualTo(Winner.player(oPlayer));
    }

    @Test
    public void XWinsWhenFillingTheSecondDiagonaleOnA3x3Board() {
        // Given
        String board =
                "  X\n" +
                " X \n" +
                "X  \n";

        // When
        Winner actualWinner = parse(board).findWinner();

        // Then
        assertThat(actualWinner).isEqualTo(Winner.player(xPlayer));
    }

    @Test
    public void XWinsWhenFillingALineOnA4x4Board() {
        // Given
        String board =
                "XXXX\n" +
                "    \n" +
                "    \n" +
                "    \n";

        // When
        Winner actualWinner = parse(board).findWinner();

        // Then
        assertThat(actualWinner).isEqualTo(Winner.player(xPlayer));
    }

    private static Board parse(String board) {
        String[] rows = board.split("\n");
        Cell[][] cells = new Cell[rows.length][];
        int rowNumber = 0;
        for (String row : rows) {
            int width = row.length();
            cells[rowNumber] = new Cell[width];
            int colNumber = 0;
            for (char col : row.toCharArray()) {
                cells[rowNumber][colNumber] = cell(String.valueOf(col));
                colNumber++;
            }
            rowNumber++;
        }

        return new Board(cells);
    }
}
