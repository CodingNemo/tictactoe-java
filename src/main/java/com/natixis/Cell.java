package com.natixis;

import java.util.Objects;

public class Cell {
    private String player;

    static Cell cell(final String player){
        return new Cell(player);
    }

    private Cell(final String player) {
        this.player = player;
    }

    public String getPlayer() {
        return player;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return Objects.equals(player, cell.player);
    }

    @Override
    public int hashCode() {
        return Objects.hash(player);
    }
}
