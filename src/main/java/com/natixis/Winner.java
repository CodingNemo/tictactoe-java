package com.natixis;

import java.util.Objects;

import static com.natixis.Players.*;

public class Winner {
    private String player;

    private Winner(String player) {
        this.player = player;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Winner winner = (Winner) o;
        return Objects.equals(player, winner.player);
    }

    @Override
    public int hashCode() {
        return Objects.hash(player);
    }

    public static Winner player(String player) {
        return new Winner(player);
    }

    public static Winner nobody() {
        return new Winner(nobody);
    }

    public Winner Or(Winner other) {
        if(this.isNobody()) {
            return other;
        }

        return this;
    }

    boolean isNobody() {
        return nobody.equals(this.player);
    }
}
