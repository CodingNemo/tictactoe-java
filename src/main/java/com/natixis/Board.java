package com.natixis;

import static com.natixis.Players.*;

class Board {
    private final int height;
    private final int width;

    private Cell[][] cells;

    Board(Cell[]... cells) {
        this.cells = cells;
        this.height = cells.length;
        this.width = cells[0].length;
    }

    Winner findWinner() {
        return
                findWinnerOnRows()
                        .Or(findWinnerOnColumns())
                        .Or(findWinnerOnFirstDiagonal())
                        .Or(findWinnerOnSecondDiagonal());

    }

    private Winner findWinnerOnFirstDiagonal() {

        Cell[] firstDiagonal = new Cell[this.width];

        for (int index = 0; index < this.width; index++) {
            firstDiagonal[index] = cells[index][index];
        }

        return findWinner(firstDiagonal);
    }

    private Winner findWinnerOnSecondDiagonal() {
        Cell[] secondDiagonal = new Cell[this.width];

        for (int index = 0; index < this.width; index++) {
            secondDiagonal[index] = cells[index][this.width - index - 1];
        }

        return findWinner(secondDiagonal);
    }


    private Winner findWinnerOnColumns() {
        Winner winner = Winner.nobody();

        for (Cell[] column : columns()) {
            winner = winner.Or(this.findWinner(column));
        }

        return winner;
    }

    private Winner findWinnerOnRows() {
        Winner winner = Winner.nobody();

        for (Cell[] row : this.cells) {
            winner = winner.Or(findWinner(row));
        }
        return winner;
    }

    private Cell[][] columns() {
        Cell[][] columns = new Cell[this.width][];

        for (int col = 0; col < this.width; col++) {
            columns[col] = col(col);
        }

        return columns;
    }

    private Cell[] col(int col) {
        Cell[] colCells = new Cell[this.height];
        for (int row = 0; row < this.height; row++) {
            colCells[row] = cells[row][col];
        }
        return colCells;
    }

    private Winner findWinner(Cell[] row) {
        String previousPlayer = null;
        for (Cell cell : row) {
            if (previousPlayer != null && !cell.getPlayer().equals(previousPlayer))
                return Winner.nobody();
            previousPlayer = cell.getPlayer();
        }

        return Winner.player(previousPlayer);
    }

}
